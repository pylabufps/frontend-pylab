import { axiosBase } from '../../api/axios-base'
const state = {
    
};
const getters = {

};
const mutations = {
    updateLocalStorage (state, { access, refresh }) {
        localStorage.setItem('access_token', access)
        localStorage.setItem('refresh_token', refresh)
        state.accessToken = access
        state.refreshToken = refresh
      },

};

const actions = {
registerUser (context, data) {
    return new Promise((resolve, reject) => {
        axiosBase.post('/register', {
        name: data.name,
        email: data.email,
        username: data.username,
        password: data.password,
        confirm: data.confirm
        })
        .then(response => {
            resolve(response)
        })
        .catch(error => {
            reject(error)
        })
    })
    },
    iniciarSesionUsuario (context, credentials) {
    return new Promise((resolve, reject) => {
        // send the username and password to the backend API:
        axiosBase.post('/api/token/', {
        username: credentials.username,
        password: credentials.password
        })
        // if successful update local storage:
        .then(response => {
            context.commit('updateLocalStorage', { access: response.data.access, refresh: response.data.refresh }) // store the access and refresh token in localstorage
            resolve()
        })
        .catch(err => {
            reject(err)
        })
    })
    }
}

export default {
state,
getters,
mutations,
actions
}