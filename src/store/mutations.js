const mutations = {
  
    updateAccess (state, access) {
    state.accessToken = access
    },

    destroyToken (state) {
    state.accessToken = null
    state.refreshToken = null
    }
};

export default mutations
