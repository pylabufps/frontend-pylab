var loginMixin = {
    data () {
        return{
        signUp: false,
        username: '',
        contraseña: '',
        correo:'',
        wrongCred: false, // activates appropriate message if set to true
        registroExitoso:''
        }
    },
    methods: {
        iniciarSesionUsuario () { // call loginUSer action
            this.$store.dispatch('iniciarSesionUsuario', {
                username: this.username,
                password: this.contraseña
            })
                .then(() => {
                    this.wrongCred = false
                    this.$router.push({ name: 'home' })
                })
                .catch(err => {
                console.log(err)
                this.wrongCred = true // if the credentials were wrong set wrongCred to true
                })
        },
        
        registroUsario () { // call loginUSer action
        this.$store.dispatch('registroUsario', {
            username: this.username,
            email: this.correo,
            password: this.contraseña
        })
            .then(() => {
                this.registroExitoso = true
            })
            .catch(err => {
            console.log(err)
                this.registroExitoso = false
            })
        },
    }
};
 
export default loginMixin